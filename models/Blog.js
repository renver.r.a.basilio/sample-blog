const mongoose = require('mongoose')
const Schema = mongoose.Schema

let BlogSchema = new Schema({
    title: {
        type: String,
        required: true,
    },
    author: {
        type: String,
        required: true,
    },
    content: {
        type: String,
        required: true,
    },
    images: {
        type: [String],
        required: true,
    },
    isCompleted: {
        type: String,
        required: true
    }
}, {
    timestamps: true
})

module.exports = mongoose.model('Blog', BlogSchema)