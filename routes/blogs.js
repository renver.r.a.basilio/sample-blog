const Blog = require('../models/Blog')
const express = require('express')
const _ = require('lodash')
const router = express.Router()

// view all blogs

router.get('/', (req, res) => {

    Blog.find({}, function(error, blogs){
        return res.json(blogs)
    })
})

// view single blog

router.get('/:id', (req, res) => {

    Blog.findById(req.params.id, function(error, blog) {
        return res.json(blog)
    })

})

// add blogs

router.post('/', (req, res) => {

    let blog = new Blog()

    blog.title = req.body.title,
    blog.author = req.body.author,
    blog.content = req.body.content,
    blog.isCompleted = req.body.isCompleted
    blog.images = req.body.images

    Blog.findOne({
        title: req.body.title
    }, function(error, blog) {

        if(blog) {
            return res.send(`${blog.title} already exists`)
        } else {
            let blog = new Blog(req.body)
            blog.save(error => {
                return res.json(blog)
            })
        }

    })

})

// update blog

router.put('/:id', (req, res) => {

    const blog = {}
    blog.title = req.body.title,
    blog.author = req.body.author,
    blog.content = req.body.content,
    blog.images = req.body.images

    Blog.findOneAndUpdate(
        {_id: req.params.id },
        blog,
        {new: true},
        function(error, newBlog) {
            return res.json(newBlog)
        }
    )
})

// delete blog

router.delete('/:id', (req, res) => {
    Blog.findOneAndDelete({_id: req.params.id}, function(error) {
        return res.send('Blog deleted!')
    })
})

// multiple image uploads

router.post('/:id/images', (req, res) => {

    if(!req.files) {
        res.send({
            status: false,
            message: 'No files uploaded'
        })
    } else {
        let data = []

        _.forEach(_.keysIn(req.files.photos), (key) => {
            let photo = req.files.photos[key];
            
            photo.mv('./images/' + photo.name);

            data.push(`/images/${photo.name}`)
        });

        const blog = {}

        blog.images = data

        Blog.findByIdAndUpdate(
            {_id: req.params.id },
            blog,
            {new: true},
            function(error, newBlog) {
                return res.json(newBlog)
            }
        )

        res.send({
            status: true,
            message: 'Files are uploaded',
            data: data
        })
    }
})

// filtering

// router.get('/blogs/', function(req, res){
//     let response = [];

//     if(req.isCompleted != 'undefined' ){
//         response = blogs.filter(function(blog){
//             if(blog.isCompleted === true){
//                 return blog;
//             }
//         });
//     } else {
//         response = blogs;
//     }

//     res.json(response);
// });

module.exports = router