const express = require('express')
const fileUpload = require('express-fileupload')
const bodyParser = require('body-parser')
const app = express()
const port = 5000
const mongoose = require('mongoose')

// enable files upload

app.use(fileUpload({
    createParentPath: true
}))

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: true}))

mongoose.connect("mongodb+srv://sample-blog:redcoresolutions@sample-blog.faorv.mongodb.net/sample-blog?retryWrites=true&w=majority",
    {useNewUrlParser: true, useUnifiedTopology: true}
)

mongoose.connection.once('open', () => {
	console.log('Now connected to MongoDB Atlas server.')
})

app.use(express.json())

    app.use("/blogs", require("./routes/blogs"))

app.listen(port,
    () => console.log(`Blog app is running on port ${port}`)    
)